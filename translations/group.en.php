<?php
return [
    'form' => [
        'save' => 'Save',
        'person' => [
            'title' => 'Title'
        ]
    ],
    'Group' => 'Group',
    'Enlarge' => 'Enlarge',
    'Description' => 'Description',
    'Upload Image' => 'Upload Image',
    'Group Information' => 'Group Information',
    'All about' => 'All about',
    'Information' => 'Information',
    'Name' => 'Group Name',
    'Contacts' => 'Contacts',
    'Icon' => 'Icon',
    'Special' => 'Special',
    'Yes' => 'Yes',
    'No' => 'No',
    'Back to group overview' => 'Back to group overview'

];
