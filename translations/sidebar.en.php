<?php

return [
    'My Information' => 'My Information',

    'Contacts' => 'Contacts',
    'New Contact' => 'New Contact',
    'All Contacts' => 'All Contacts',

    'Groups' => 'Groups',
    'New Group' => 'New Group',
    'All Groups' => 'All Groups',

    'Settings' => 'Settings',
    'Logout' => 'Logout',
];
