<?php

return [
    'My Information' => 'Mijn Informatie',

    'Contacts' => 'Contactpersonen',
    'New Contact' => 'Nieuwe Contactpersoon',
    'All Contacts' => 'Alle Contactpersonen',

    'Groups' => 'Groep',
    'New Group' => 'Nieuwe Groep',
    'All Groups' => 'Alle Groepen',

    'Settings' => 'Instellingen',
    'Logout' => 'Uitloggen',
];
