$('.menu #nav-contacts')
    .popup({
        inline     : true,
        hoverable  : false,
        position   : 'bottom center',
        delay: {
            show: 300,
            hide: 800
        }
    })
;