$(document).ready(function () {
    $('.scale.cards .card')
        .transition({
            animation: 'scale in',
            interval: 120
        });
});