<?php

namespace App\Controller;

use App\Entity\Grouping;
use App\Form\GroupingType;
use App\Repository\GroupingRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Person;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * @Route("/group")
 */
class GroupingController extends AbstractController
{
    /**
     * @param GroupingRepository $groupRepository
     * @Route("/", name="group_index", methods="GET")
     * @return Response
     */
    public function index(GroupingRepository $groupRepository): Response
    {
        return $this->render(
            'group/all.html.twig',
            [
                'groupings' => $groupRepository->findBy(['special' => false]),
                'groupingsSpecial' => $groupRepository->findBy(['special' => true]),
            ]
        );
    }

    /**
     * @param GroupingRepository $groupRepository
     * @Route("/mine", name="group_mine", methods="GET")
     * @return Response
     */
    public function myGrouping(GroupingRepository $groupRepository): Response
    {
        return $this->render(
            'group/specific.html.twig',
            [
                'groupings' => $groupRepository->findBy(['special' => false]),
            ]
        );
    }

    /**
     * @param GroupingRepository $groupRepository
     * @Route("/special", name="group_special", methods="GET")
     * @return Response
     */
    public function specialGrouping(GroupingRepository $groupRepository): Response
    {
        return $this->render(
            'group/specific.html.twig',
            [
                'groupings' => $groupRepository->findBy(['special' => true]),
            ]
        );
    }

    /**
     * @param Request $request
     * @Route("/new", name="group_new", methods="GET|POST")
     * @return Response
     */
    public function new(Request $request): Response
    {
        $group = new Grouping();
        $form = $this->createForm(GroupingType::class, $group);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($group);
            $entityManager->flush();

            return $this->redirectToRoute('group_index');
        }

        return $this->render('group/new.html.twig', [
            'group' => $group,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @param Grouping $grouping
     * @Route("/{id}", name="group_show", methods="GET")
     * @return Response
     */
    public function show(Grouping $grouping): Response
    {
        return $this->render('group/show.html.twig', ['grouping' => $grouping]);
    }

    /**
     * @param Request $request
     * @param Grouping $grouping
     * @param ValidatorInterface $validator
     * @Route("/{id}/edit", name="group_edit", methods="GET|POST")
     * @return Response
     */
    public function edit(Request $request, Grouping $grouping, ValidatorInterface $validator): Response
    {
        $repository = $this->getDoctrine()->getRepository(Person::class);
        // Collect only the IDs of persons in this group.
        $persons = '';
        foreach ($grouping->getPeople()->getValues() as $person) {
            $persons .= $person->getId() . ',';
        }

        $form = $this->createForm(GroupingType::class, $grouping);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $grouping->setIcon($form->getExtraData()['icon']);
            $grouping->setColor($form->getExtraData()['color']);

            $newPersons = [];
            foreach (explode(',', $form->getExtraData()['persons']) as $index => $person) {
                $newPersons[$index] = $repository->findOneBy(['id' => $person]);
            }
            $grouping->updatePersons($newPersons);

            $this->getDoctrine()->getManager()->flush();
            return $this->redirectToRoute('group_show', ['id' => $grouping->getId()]);
        }

        return $this->render('group/edit.html.twig', [
            'grouping' => $grouping,
            'persons' => $repository->findAll(),
            'personsInGrouping' => substr($persons, 0, -1),
            'form' => $form->createView(),
        ]);
    }

    /**
     * @param Request $request
     * @param Grouping $group
     * @Route("/{id}", name="group_delete", methods="DELETE")
     * @return Response
     */
    public function delete(Request $request, Grouping $group): Response
    {
        if ($this->isCsrfTokenValid('delete' . $group->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($group);
            $entityManager->flush();
        }

        return $this->redirectToRoute('group_index');
    }
}
