<?php

namespace App\Controller;

use App\Entity\Person;
use App\Form\PersonType;
use App\Repository\PersonRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

class PersonController extends AbstractController
{

    /**
     * @param PersonRepository $personRepository
     * @param Request $request
     * @Route("/all/{page}", name="person_index",
     * methods="GET|POST", requirements={"page"="\d+"})
     * @IsGranted("ROLE_ROOT")
     * @return Response
     */
    public function list(PersonRepository $personRepository, Request $request, $page = 1): Response
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        $locale = $request->getLocale();
        $limit = $request->get('limit') ?? 15;

        return $this->render(
            'person/index.html.twig',
            [
                'people' => $personRepository->findAllPagination($page, $limit),
                'user' => $this->getUser(),
                'page' => $page,
                'pagination' => ceil((float)$personRepository->countAll()[0]['1'] / $limit)
            ]
        );
    }

    /**
     * @param Request $request
     * @Route("/new", name="person_new", methods="GET|POST")
     * @IsGranted("ROLE_ROOT")
     * @return Response
     */
    public function new(Request $request): Response
    {
        $person = new Person();
        $form = $this->createForm(PersonType::class, $person);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($person);
            $entityManager->flush();

            return $this->redirectToRoute('person_index');
        }

        return $this->render('person/new.html.twig', [
            'person' => $person,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @param Person $person
     * @Route("/{id}", name="person_show", methods="GET", requirements={"id"="\d+"} )
     * @IsGranted("ROLE_ROOT")
     * @return Response
     */
    public function show(Person $person): Response
    {
        return $this->render(
            'person/show.html.twig',
            [
                'person' => $person,
                'related' => $this->getDoctrine()->getRepository(Person::class)->findByRelated($person),
                'relatedGroups' =>
                    $this->getDoctrine()->getRepository(Person::class)
                    ->findByGrouping($person->getId(), $person->getGroupings()->getValues())
            ]
        );
    }

    /**
     * @param Request $request
     * @param Person $person
     * @Route("/{id}/edit", name="person_edit", methods="GET|POST")
     * @IsGranted("ROLE_ROOT")
     * @return Response
     */
    public function edit(Request $request, Person $person): Response
    {
        $form = $this->createForm(PersonType::class, $person);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('person_show', ['id' => $person->getId()]);
        }

        return $this->render('person/edit.html.twig', [
            'person' => $person,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @param Request $request
     * @param Person $person
     * @Route("/{id}", name="person_delete", methods="DELETE")
     * @IsGranted("ROLE_ROOT")
     * @return Response
     */
    public function delete(Request $request, Person $person): Response
    {
        if ($this->isCsrfTokenValid('delete' . $person->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($person);
            $entityManager->flush();
        }

        return $this->redirectToRoute('person_index');
    }
}
