<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181124221733 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE person ADD facebook VARCHAR(50) DEFAULT NULL, ADD instagram VARCHAR(100) DEFAULT NULL, ADD googleplus VARCHAR(100) DEFAULT NULL, ADD twitter VARCHAR(100) DEFAULT NULL, ADD snapchat VARCHAR(100) DEFAULT NULL, ADD pinterest VARCHAR(100) DEFAULT NULL, ADD linkedin VARCHAR(100) DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE person DROP facebook, DROP instagram, DROP googleplus, DROP twitter, DROP snapchat, DROP pinterest, DROP linkedin');
    }
}
