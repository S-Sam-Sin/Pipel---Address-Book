<?php

namespace App\Repository;

use App\Entity\Grouping;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Grouping|null find($id, $lockMode = null, $lockVersion = null)
 * @method Grouping|null findOneBy(array $criteria, array $orderBy = null)
 * @method Grouping[]    findAll()
 * @method Grouping[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class GroupingRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Grouping::class);
    }

    /**
     * @return Grouping[] Returns an array of Group objects
     */
    public function findByGrouping($value): array
    {
        return $this->createQueryBuilder('g')
            ->andWhere('g.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('g.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult();
    }

    /**
     * @param int $groupId
     * @param array $people
     * @return Grouping[] Returns an array of Group objects
     */
    public function findPersonsIds(int $groupId, array $people): array
    {
        return $this->createQueryBuilder('g')
            ->innerJoin('g.people', 'p')
            ->addSelect('p.id')
            ->andWhere('p.id IN (:people)')
            ->andWhere('g.id = (:groupId)')
            ->orderBy('p.id', 'ASC')
            ->setParameter('people', $people)
            ->setParameter('groupId', $groupId)
            ->getQuery()
            ->getResult();
    }


    /*
    public function findOneBySomeField($value): ?Group
    {
        return $this->createQueryBuilder('g')
            ->andWhere('g.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
