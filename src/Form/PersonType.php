<?php

namespace App\Form;

use App\Entity\Person;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class PersonType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options) :void
    {
        $domain = 'form';
        $format = 'form.person.';

        $builder
            //Personal Information
            ->add('title', TextType::class, [
                'required' => false,
                'translation_domain' => $domain,
                'label_format' => $format.'%name%',
                'help' => 'A title is a prefix or suffix added to someone\'s name in 
                certain contexts. It may signify either veneration, an official position
                 or a professional or academic qualification.',
            ])
            ->add('firstName', TextType::class, [
                'required' => true,
                'translation_domain' => $domain,
                ])
            ->add('middleName', TextType::class, [
                'required' => false,
                'translation_domain' => $domain,
            ])
            ->add('lastName', TextType::class, [
                'required' => false,
                'translation_domain' => $domain,
            ])
            ->add('gender', ChoiceType::class, [
                'required' => true,
                'choice_translation_domain' => $domain,
                'placeholder' => 'Male or Female?',
                'choices' => [
                    'Male' => 1,
                    'Female' => 0
                ]
            ])
            ->add('address', TextType::class, [
                'required' => false,
                'translation_domain' => $domain,
            ])
            ->add('birthday', DateType::class, [
                'required' => false,
                'choice_translation_domain' => $domain,
                'placeholder' => 'When was ... born',
            ])
            ->add('description', TextareaType::class, [
                'required' => false,
                'translation_domain' => $domain,
            ])
            //Connectivity Information
            ->add('phoneNumber', TelType::class, [
                'required' => false,
                'translation_domain' => $domain,
            ])
            ->add('email', TelType::class, [
                'required' => false,
                'translation_domain' => $domain,
            ])
            ->add('twitter', TextType::class, [
                'required' => false,
                'translation_domain' => $domain,
            ])
            ->add('facebook', TextType::class, [
                'required' => false,
                'translation_domain' => $domain,
            ])
            ->add('instagram', TextType::class, [
                'required' => false,
                'translation_domain' => $domain,
            ])
            ->add('googleplus', TextType::class, [
                'required' => false,
                'translation_domain' => $domain,
            ])
            ->add('snapchat', TextType::class, [
                'required' => false,
                'translation_domain' => $domain,
            ])
            ->add('pinterest', TextType::class, [
                'required' => false,
                'translation_domain' => $domain,
            ])
            ->add('linkedin', TextType::class, [
                'required' => false,
                'translation_domain' => $domain,
            ])
            ->add('save', SubmitType::class, [
                'translation_domain' => $domain,
                'label_format' => 'form.%name%',
                'attr' => [
                    'class' => 'ui right floated teal button'
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver) :void
    {
        $resolver->setDefaults([
            'data_class' => Person::class,
        ]);
    }
}
