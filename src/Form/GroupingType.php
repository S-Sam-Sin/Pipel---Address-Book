<?php

namespace App\Form;

use App\Entity\Grouping;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use App\Entity\Person;

class GroupingType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options) :void
    {
        $domain = 'form';
        $format = 'form.person.';

        $builder
            ->add('name', TextType::class)
            ->add('save', SubmitType::class, [
                'translation_domain' => $domain,
                'label_format' => 'form.%name%',
                'attr' => [
                    'class' => 'ui right floated teal button'
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver) :void
    {
        $resolver->setDefaults([
            'data_class' => Grouping::class,
            'allow_extra_fields' => true
        ]);
    }
}
