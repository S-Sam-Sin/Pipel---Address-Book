<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\Person;
use Faker\Factory;
use Carbon\Carbon;

class PersonFixtures extends Fixture
{
    public function load(ObjectManager $manager) :void
    {
        $faker = Factory::create();
        $genderOptions = ['male', 'female'];
        $gender = $genderOptions[random_int(0, 1)];

        $demoPersons = [
            0 => [
                'gender' => 0,
                'title' => $faker->title('female'),
                'name' => $faker->firstName('female'),
                'slug' => '1'
            ],
            1 => [
                'gender' => 0,
                'title' => $faker->title('female'),
                'name' => $faker->firstName('female'),
                'slug' => '2'
            ],
            2 => [
                'gender' => 0,
                'title' => $faker->title('female'),
                'name' => $faker->firstName('female'),
                'slug' => '3'
            ],
            3 => [
                'gender' => 1,
                'title' => $faker->title('male'),
                'name' => $faker->firstName('male'),
                'slug' => '4'
            ],
            4 => [
                'gender' => 0,
                'title' => $faker->title('female'),
                'name' => $faker->firstName('female'),
                'slug' => '5'
            ],
            5 => [
                'gender' => 0,
                'title' => $faker->title('female'),
                'name' => $faker->firstName('female'),
                'slug' => '6'
            ],
            6 => [
                'gender' => 1,
                'title' => $faker->title('male'),
                'name' => $faker->firstName('male'),
                'slug' => '7'
            ],
            7 => [
                'gender' => 0,
                'title' => $faker->title('female'),
                'name' => $faker->firstName('female'),
                'slug' => '8'
            ],
            8 => [
                'gender' => 1,
                'title' => $faker->title('male'),
                'name' => $faker->firstName('male'),
                'slug' => '9'
            ],
            9 => [
                'gender' => 0,
                'title' => $faker->title('female'),
                'name' => $faker->firstName('female'),
                'slug' => '10'
            ],
        ];

        foreach ($demoPersons as $demoPerson) {
            $person = new Person();
            $person->setGender($demoPerson['gender']);
            $person->setTitle($demoPerson['title']);
            $person->setFirstName($demoPerson['name']);
            $person->setLastName($faker->lastName());
            $person->setBirthday(Carbon::now());
            $person->setAddress($faker->address);
            $person->setEmail($faker->safeEmail);
            $person->setFacebook($faker->optional($weight = 0.5, $default = null)->userName);
            $person->setInstagram($faker->optional($weight = 0.5, $default = null)->userName);
            $person->setGoogleplus($faker->optional($weight = 0.5, $default = null)->userName);
            $person->setSnapchat($faker->optional($weight = 0.5, $default = null)->userName);
            $person->setPinterest($faker->optional($weight = 0.5, $default = null)->userName);
            $person->setLinkedin($faker->optional($weight = 0.5, $default = null)->userName);
            $person->setSlug($demoPerson['slug']);
            $person->setPhoneNumber($faker->e164PhoneNumber);
            $person->setDescription($faker->realText);
            $manager->persist($person);
        }

        for ($i = 0; $i <= 11; $i++) {
            $person = new Person();
            $person->setGender($faker->boolean($chanceOfGettingTrue = 50));
            $person->setTitle($faker->title($gender));
            $person->setFirstName($faker->firstName($gender));
            $person->setLastName($faker->lastName());
            $person->setBirthday(Carbon::now());
            $person->setAddress($faker->address);
            $person->setEmail($faker->safeEmail);
            $person->setFacebook($faker->optional($weight = 0.5, $default = null)->userName);
            $person->setInstagram($faker->optional($weight = 0.5, $default = null)->userName);
            $person->setGoogleplus($faker->optional($weight = 0.5, $default = null)->userName);
            $person->setSnapchat($faker->optional($weight = 0.5, $default = null)->userName);
            $person->setPinterest($faker->optional($weight = 0.5, $default = null)->userName);
            $person->setLinkedin($faker->optional($weight = 0.5, $default = null)->userName);
            $person->setSlug('test-person');
            $person->setPhoneNumber($faker->e164PhoneNumber);
            $person->setDescription($faker->realText);
            $manager->persist($person);
        }

        $manager->flush();
    }
}
