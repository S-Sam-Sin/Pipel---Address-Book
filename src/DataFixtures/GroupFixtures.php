<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\Grouping;

class GroupFixtures extends Fixture
{
    public function load(ObjectManager $manager) :void
    {
        // Standard Groups
        $grouping = new Grouping();
        $grouping->setName('Friends');
        $grouping->setIcon('handshake outline');
        $grouping->setColor('blue');
        $grouping->setIntro('Overcome key issues to meet key milestones commitment to the cause.');
        $grouping->setDescription('Where do we stand on the latest client ask this is not the hill i want to die on, but UX guerrilla marketing. Baseline the procedure and samepage your department. Out of scope shelfware, but bells and whistles, so enough to wash your face. Touch base drill down, but prioritize these line items so ladder up / ladder back to the strategy baseline the procedure and samepage your department.');
        $grouping->setSpecial(false);
        $manager->persist($grouping);

        $grouping = new Grouping();
        $grouping->setName('Family');
        $grouping->setIcon('tint');
        $grouping->setColor('red');
        $grouping->setIntro('Overcome key issues to meet key milestones commitment to the cause.');
        $grouping->setDescription('Where do we stand on the latest client ask this is not the hill i want to die on, but UX guerrilla marketing. Baseline the procedure and samepage your department. Out of scope shelfware, but bells and whistles, so enough to wash your face. Touch base drill down, but prioritize these line items so ladder up / ladder back to the strategy baseline the procedure and samepage your department.');
        $grouping->setSpecial(false);
        $manager->persist($grouping);

        $grouping = new Grouping();
        $grouping->setName('Coworkers');
        $grouping->setIcon('boxes');
        $grouping->setColor('brown');
        $grouping->setIntro('Overcome key issues to meet key milestones commitment to the cause.');
        $grouping->setDescription('Where do we stand on the latest client ask this is not the hill i want to die on, but UX guerrilla marketing. Baseline the procedure and samepage your department. Out of scope shelfware, but bells and whistles, so enough to wash your face. Touch base drill down, but prioritize these line items so ladder up / ladder back to the strategy baseline the procedure and samepage your department.');
        $grouping->setSpecial(false);
        $manager->persist($grouping);

        //Special Groups
        $grouping = new Grouping();
        $grouping->setName('Facebook');
        $grouping->setIcon('facebook');
        $grouping->setColor('blue');
        $grouping->setSpecial(true);
        $manager->persist($grouping);

        $grouping = new Grouping();
        $grouping->setName('Twitter');
        $grouping->setIcon('twitter');
        $grouping->setColor('blue');
        $grouping->setSpecial(true);
        $manager->persist($grouping);

        $grouping = new Grouping();
        $grouping->setName('Instagram');
        $grouping->setIcon('instagram');
        $grouping->setColor('orange');
        $grouping->setSpecial(true);
        $manager->persist($grouping);

        $grouping = new Grouping();
        $grouping->setName('Google +');
        $grouping->setIcon('google plus g');
        $grouping->setColor('red');
        $grouping->setSpecial(true);
        $manager->persist($grouping);

        $grouping = new Grouping();
        $grouping->setName('Snapchat');
        $grouping->setIcon('snapchat ghost');
        $grouping->setColor('yellow');
        $grouping->setSpecial(true);
        $manager->persist($grouping);

        $grouping = new Grouping();
        $grouping->setName('Pinterest');
        $grouping->setIcon('pinterest p');
        $grouping->setColor('red');
        $grouping->setSpecial(true);
        $manager->persist($grouping);

        $grouping = new Grouping();
        $grouping->setName('Linkedin');
        $grouping->setIcon('linkedin');
        $grouping->setColor('blue');
        $grouping->setSpecial(true);
        $manager->persist($grouping);

        $manager->flush();
    }
}
